# TidsBank FrontEnd

### Tidsbanken Api
As a user or admin, you can use this system to request your vacation from your current company.

Git-link to API code: (https://gitlab.com/atmaratmar/tidsbankapi)
### Contributors
- Mosiur 
- Ramesh 
- Atmar

### Presentation Link:

-  (https://docs.google.com/presentation/d/1izLdfLi-kQH3VouJNXMcZllME-hO4xxPAE1Fve9L9EE/edit?usp=sharing)

### Pre/requisites
VsCode 2019 

### How to Start
Clone Project in to local mechine 
click on .sln file in the folder

### Documentation

- API Documentation: (https://drive.google.com/file/d/1AYP7d2eJmtxgsSgDH-6isnENJk-6X3nI/view?usp=sharing)

- User Manual: ( https://drive.google.com/file/d/1A3hrFk_7Y4cjQ0UWTC8bhYWKNSK8Zgwr/view?usp=sharing)

- Admin Manual: (https://drive.google.com/file/d/1hg3nYBAAzXbXQb7-b2hdYCyWFm7JJr_m/view?usp=sharing)

### Demo of Tidsbanken Project
Go to the following link and click login after that write user name password for access the system.

- **For Admin**
user name : admin@admin.com 
pwd:admin123


- **For user**
user name : user@user.com pwd:user123

(https://mytidsbankenapp.azurewebsites.net/)
